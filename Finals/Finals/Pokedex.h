#ifndef POKEDEX_H
#define POKEDEX_H

#include <string>

using namespace std;

class Pokedex
{
public:

	void whichPokemon();

	string name;
	int hp;
	int attack;
	int pokeIndex;
};
#endif
