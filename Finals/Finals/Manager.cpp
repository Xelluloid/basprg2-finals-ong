#include "Manager.h"
#include "Trainer.h"
#include "Location.h"
#include <iostream>

using namespace std;

void Manager::display(Trainer* player)
{
	cout << "Hi there trainer! What's your name?" << endl;
	cin >> player->trainer;

	system("pause");
	system("cls");

	cout << "I currently have (3) Pokemon for you to choose from!" << endl;
	cout << "The one you choose will accompany you to your journey!" << endl;
	cout << "Pick one now!" << endl << endl;
	cout << "[1] - Bulbausaur		Grass Type		Level 5" << endl;
	cout << "[2] - Charmander		Fire Type		Level 5" << endl;
	cout << "[3] - Squirtle			Water Type		Level 5" << endl << endl;
	cout << "Input your choice: ";
	cin >> choice;

	switch (choice)
	{
	case 1:
		cout << "You have chosen Bulbasaur!" << endl;
		break;
	case 2:
		cout << "You have chosen Charmander!" << endl;
		break;
	case 3:
		cout << "You have chosen Squirtle!" << endl;
		break;
	}

	system("pause");
	system("cls");


	int x = 0;
	int y = 0;

	while (true)
	{
		cout << "What would you like to do?" << endl;
		cout << "[1] - Move					[2] - Pokemon" << endl;
		cout << "[3] - Pokemon Center" << endl;
		cin >> choice2;

		switch (choice2)
		{
		case 1:
			cout << "Where would you like to go?" << endl;
			cout << "[w] - Up			[a] - Left" << endl;
			cout << "[s] - Down			[d] - Right" << endl;

			cin >> decision;

			switch (decision)
			{
			case 'w':
				y++;
				break;
			case 's':
				y--;
				break;
			case 'a':
				x++;
				break;
			case 'd':
				x--;
				break;
			default:
				"";
			}

			cout << "Your current location: (" << x << ", " << y << ")" << endl;

			break;
		case 2:
			"";
			break;
		case 3:
			cout << "Welcome to the Pokemon Center!" << endl;
			cout << "Would you like your Pokemon to be healed up? (y/n)";
			cin >> choice3;

			switch (choice3)
			{
			case 'y':
				cout << "We have healed up your Pokemon! Hope to see you again!" << endl;
				break;
			case 'n':
				cout << "We hope to see you again!" << endl;
				break;
			}
		}
		
		system("pause");
		system("cls");
	}

}