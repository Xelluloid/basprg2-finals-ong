#ifndef MANAGER_H
#define MANAGER_H
#include "Trainer.h"
#include "Pokemon.h"
#include "Location.h"
#include <string>

using namespace std;

class Manager: public Trainer
{
public:
	void display(Trainer* player);
};
#endif